(function($, document) {

    var pltfrm_panamy_deliveries = {

        cache: function() {

            pltfrm_panamy_deliveries.els = {};
            pltfrm_panamy_deliveries.vars = {};

            // common vars
            // pltfrm_panamy_deliveries.vars.a_variable = 1;

            // common elements
            pltfrm_panamy_deliveries.els.complete_buttons = $('.pltfrm-complete');
            pltfrm_panamy_deliveries.els.loading = $('.pltfrm-deliveries-loading');
            pltfrm_panamy_deliveries.els.print_buttons = $('.pltfrm-print-labels');
            pltfrm_panamy_deliveries.els.remove_flag_buttons = $('.pltfrm-remove-flag');
            pltfrm_panamy_deliveries.els.print_deliveries_button = $('.print-deliveries .button');

        },

        on_ready: function() {

            // on ready stuff here
            pltfrm_panamy_deliveries.cache();
            pltfrm_panamy_deliveries.setup_buttons();
            pltfrm_panamy_deliveries.setup_filters();

        },

        setup_buttons: function() {
            this.setup_complete_buttons();
            this.setup_print_buttons();
            this.setup_remove_flag_buttons();
            this.setup_print_deliveries_button();
        },

        setup_print_deliveries_button: function(){

            pltfrm_panamy_deliveries.els.print_deliveries_button.on('click', function(e){
                e.preventDefault();
                window.print();
            });

        },

        setup_filters: function(){

            // Bind the date field as a date picker
            $('.filter-deliveries input[type="date"]').datepicker({
                dateFormat: 'yy-mm-dd',
                showButtonPanel: true,
                onSelect: function() {
                    var $next = $(this).next();
                    var $prev = $(this).prev();
                    // If the date picker is the next element then we've selected the first picker
                    // and should only apply the same date to the picker if it's already empty
                    if ($next.hasClass('hasDatepicker')){
                        if (!$next.val()){
                            $next.val($(this).val());
                        }
                    } else if ($prev.hasClass('hasDatepicker')) {
                        // If it's not a date picker then we should look at the previous element and ensure a date 
                        // has been entered, essentially lazy validation at this stage
                        if (!$prev.val()){
                            $prev.val($(this).val());
                        }
                    }
                },
                gotoToday: function(){
                    console.log('Goto today');
                }
            });

            // When today is selected it should set the date
            $.datepicker._gotoToday = function(id) {
                var $next = $(id).next();
                var $prev = $(id).prev();
                $(id).datepicker('setDate', new Date()).datepicker('hide').blur(); 
                if ($next.hasClass('hasDatepicker')){
                    if (!$next.val()){
                        $next.val($(id).val());
                    }
                } else if ($prev.hasClass('hasDatepicker')){
                    if (!$prev.val()){
                        $prev.val($(id).val());
                    }
                }
            };

            // Add clear button for date field
            $('.filter-deliveries .input .clear').on('click', function(e){
                e.preventDefault();
                $(this).closest('.input').find('input').val('');
            });

            // Show loading on filter click
            $('.filter-deliveries .button').on('click', function(){
                $('.filter-deliveries').addClass('loading');
            });

            $('.pltfrm-deliveries-wrap .show-filters').on('click', function(){
                $(this).parent().addClass('show-filters');
            });



        },

        setup_remove_flag_buttons: function(){

            pltfrm_panamy_deliveries.els.remove_flag_buttons.on('click', function(){

                var $this = $(this);
                var $row = $this.closest('tr');

                var data = {
                    order_id: $row.attr('data-order-id'),
                    delivery_id: $row.attr('data-delivery-id'),
                    order_parts: $row.attr('data-order-parts'),
                    recipient_name: $row.attr('data-recipient-name'),
                    recipient_address: $row.attr('data-recipient-address'),
                    recipient_flowers: $row.attr('data-recipient-flowers'),
                    customer_email: $row.attr('data-customer-email'),
                    delivery_date: $row.attr('data-delivery-date'),
                    tracking_number: $row.find('.tracking-number').val(),
                    lang: $row.attr('data-lang')
                };

                if (confirm('Are you sure you want to remove the flag from this delivery?')){

                    // Show loading spinner while we unflag the item
                    $this.find('.fa').removeClass('fa-times-circle').addClass('fa-refresh fa-spin');

                    // Flag the order via ajax
                    pltfrm_panamy_deliveries.unflag_delivery( data, function(){
                        $row.removeClass('flagged');
                        $this.find('.fa').addClass('fa-times-circle').removeClass('fa-refresh fa-spin');
                    });

                }

                return false;
                
            });

        },

        setup_print_buttons: function(){

            pltfrm_panamy_deliveries.els.print_buttons.on('click', function(){

                var $row = $(this).closest('tr');

                var data = {
                    order_id: $row.attr('data-order-id'),
                    delivery_id: $row.attr('data-delivery-id'),
                    order_parts: $row.attr('data-order-parts'),
                    recipient_name: $row.attr('data-recipient-name'),
                    recipient_address: $row.attr('data-recipient-address'),
                    recipient_flowers: $row.attr('data-recipient-flowers'),
                    customer_email: $row.attr('data-customer-email'),
                    delivery_date: $row.attr('data-delivery-date'),
                    tracking_number: $row.find('.tracking-number').val(),
                    lang: $row.attr('data-lang')
                };

                // Flag the order via ajax
                pltfrm_panamy_deliveries.flag_delivery( data, function(){
                    $row.removeClass('overdue');
                    $row.addClass('flagged');
                });
                
            });

        },

        setup_complete_buttons: function(){

            pltfrm_panamy_deliveries.els.complete_buttons.on('click', function(){

                var $row = $(this).closest('tr');

                var data = {
                    order_id: $row.attr('data-order-id'),
                    delivery_id: $row.attr('data-delivery-id'),
                    order_parts: $row.attr('data-order-parts'),
                    recipient_name: $row.attr('data-recipient-name'),
                    recipient_address: $row.attr('data-recipient-address'),
                    recipient_flowers: $row.attr('data-recipient-flowers'),
                    customer_email: $row.attr('data-customer-email'),
                    delivery_date: $row.attr('data-delivery-date'),
                    tracking_number: $row.find('.tracking-number').val(),
                    lang: $row.attr('data-lang')
                },
                $the_row = $row;

                // build "Are you sure?" message

                var message = "Are you sure you want to mark order: "+data.order_id+", package: "+data.delivery_id+" as complete";

                if( typeof data.tracking_number !== "undefined" ) {
                    if( data.tracking_number !== "" ) {
                        message += ", with a tracking number of: "+data.tracking_number+"";
                    } else {
                        message += ", with no tracking number";
                    }
                }

                message += "?";

                if( confirm( message ) ) {

                    pltfrm_panamy_deliveries.els.loading.show();

                    pltfrm_panamy_deliveries.add_delivery_completion( data, function(){

                        $the_row.remove();
                        pltfrm_panamy_deliveries.update_tr_classes( $('#pltfrm-deliveries') );
                        pltfrm_panamy_deliveries.els.loading.hide();

                    });

                }

                return false;

            });

        },

        flag_delivery: function( data, callback ) {

            data.action = 'pltfrm_flag_delivery';

            // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
            jQuery.post(ajaxurl, data, function(response) {
                if( response.success ) {
                    if (typeof callback === "function") { callback(); }
                }
            });

        },

        unflag_delivery: function( data, callback ) {

            data.action = 'pltfrm_unflag_delivery';

            // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
            jQuery.post(ajaxurl, data, function(response) {
                if( response.success ) {
                    if (typeof callback === "function") { callback(); }
                }
            });

        },

        add_delivery_completion: function( data, callback ) {

            data.action = 'pltfrm_add_delivery_completion';

            // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
            jQuery.post(ajaxurl, data, function(response) {

                if( response.success ) {
                    if (typeof callback === "function") { callback(); }
                }

            });

        },

        update_tr_classes: function( $table ) {

            if( $table.length > 0 ) {

                $table.find("tbody tr").removeClass("alternate");
                $table.find("tbody tr:even").addClass("alternate");

            }

        }

    };

	$(document).ready( pltfrm_panamy_deliveries.on_ready() );

}(jQuery, document));