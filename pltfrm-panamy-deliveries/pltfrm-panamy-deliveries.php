<?php
/*
Plugin Name: Panamy Deliveries
Plugin URI: http://www.jckemp.com
Description: Delivery management for Panamy
Version: 1.1.2
Author: James Kemp (Maintained by Hinge Ltd)
Author URI: http://www.jckemp.com
Text Domain: pltfrm-panamy-deliveries
*/

// ini_set('display_errors', 1);

class Pltfrm_Panamy_Deliveries {

    public $name = 'Panamy Deliveries';
    public $shortname = 'Deliveries';
    public $slug = 'pltfrm-panamy-deliveries';
    public $version = "1.1.1";
    public $db_version = "1.1";
    public $deliveries_table_slug = "pltfrm_deliveries";
    public $deliveries_table_name;
    public $plugin_path;
    public $plugin_url;

/** =============================
    *
    * Construct the plugin
    *
    ============================= */

    public function __construct() {

        global $wpdb;

        $this->plugin_path = plugin_dir_path( __FILE__ );
        $this->plugin_url = plugin_dir_url( __FILE__ );
        $this->deliveries_table_name = $wpdb->prefix . $this->deliveries_table_slug;

        register_activation_hook( __FILE__, array( $this, 'install' ) );

        // Hook up to the init and plugins_loaded actions
        add_action( 'plugins_loaded',   array( $this, 'plugins_loaded_hook' ) );
        add_action( 'init',             array( $this, 'initiate_hook' ) );
        add_action( 'wp',               array( $this, 'wp_hook' ) );

    }

/** =============================
    *
    * Install Plugin on Activation
    *
    ============================= */

    public function install() {

        $this->setup_deliveries_db();

    }

/** =============================
    *
    * Run quite near the start (http://codex.wordpress.org/Plugin_API/Action_Reference)
    *
    ============================= */

    public function plugins_loaded_hook() {

        $this->textdomain();
        $this->load_advanced_custom_fields();

    }

/** =============================
    *
    * Run after the current user is set (http://codex.wordpress.org/Plugin_API/Action_Reference)
    *
    ============================= */

    public function initiate_hook() {

        if(is_admin()) {

            add_action( 'admin_enqueue_scripts',                        array( $this, 'admin_scripts' ));
            add_action( 'admin_enqueue_scripts',                        array( $this, 'admin_styles' ));
            add_action( 'admin_menu',                                   array( $this, 'deliveries_page' ) );

            add_action( 'wp_ajax_pltfrm_add_delivery_completion',       array( $this, 'add_delivery_completion' ) );
            add_action( 'wp_ajax_pltfrm_flag_delivery',                 array( $this, 'flag_delivery' ) );       
            add_action( 'wp_ajax_pltfrm_unflag_delivery',                 array( $this, 'unflag_delivery' ) );            

            add_action( 'woocommerce_order_status_changed',             array( $this, 'change_delivery_status' ), 10, 3 );

        } else {

            add_action('woocommerce_new_order',                         array( $this, 'process_new_order' ), 10, 1);

        }

        add_filter( 'woocommerce_prevent_admin_access',             array( $this, 'prevent_admin_access' ), 10, 1);

    }

/** =============================
    *
    * Run after wp is fully set up and $post is accessible (http://codex.wordpress.org/Plugin_API/Action_Reference)
    *
    ============================= */

    public function wp_hook() {

        global $post;

        if(!is_admin()) {

        }

    }

/** =============================
    *
    * Load plugin textdomain
    *
    ============================= */

    public function textdomain() {

        load_plugin_textdomain( 'pltfrm-panamy-deliveries', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );

    }

/** =============================
    *
    * Load ACF Fields
    *
    ============================= */

    public function load_advanced_custom_fields() {        

        if( function_exists('acf_add_local_field_group') ):

            acf_add_local_field_group(array (
                'key' => 'group_5b069d1c6020c',
                'title' => 'Panamy Deliveries B2B Options',
                'fields' => array (
                    array (
                        'key' => 'field_5b069d262ff41',
                        'label' => 'Label Colour',
                        'name' => 'panamy_b2b_label_colour',
                        'type' => 'color_picker',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '#f1bb3b',
                    ),
                ),
                'location' => array (
                    array (
                        array (
                            'param' => 'user_form',
                            'operator' => '==',
                            'value' => 'edit',
                        ),
                    ),
                ),
                'menu_order' => 0,
                'position' => 'normal',
                'style' => 'default',
                'label_placement' => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen' => '',
                'active' => 1,
                'description' => '',
            ));

            acf_add_local_field_group(array (
                'key' => 'group_5b06a3c51750e',
                'title' => 'Panamy Deliveries',
                'fields' => array (
                    array (
                        'key' => 'field_5b06a3d7ab3ad',
                        'label' => 'B2B',
                        'name' => '',
                        'type' => 'tab',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'placement' => 'top',
                        'endpoint' => 0,
                    ),
                    array (
                        'key' => 'field_5b06a3e7ab3ae',
                        'label' => 'Roles',
                        'name' => 'panamy_b2b_roles',
                        'type' => 'text',
                        'instructions' => 'Comma Separated',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => 'customer-b2b,bvlgari',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array (
                        'key' => 'field_5b06a4e886836',
                        'label' => 'Deliveries',
                        'name' => '',
                        'type' => 'tab',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'placement' => 'top',
                        'endpoint' => 0,
                    ),
                    array (
                        'key' => 'field_5b06a4f286837',
                        'label' => 'Local Postcodes',
                        'name' => 'local_postcodes',
                        'type' => 'text',
                        'instructions' => 'Comma Separated',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array (
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                ),
                'location' => array (
                    array (
                        array (
                            'param' => 'options_page',
                            'operator' => '==',
                            'value' => 'acf-options',
                        ),
                    ),
                ),
                'menu_order' => 0,
                'position' => 'normal',
                'style' => 'default',
                'label_placement' => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen' => '',
                'active' => 1,
                'description' => '',
            ));

        endif;

    }

/** =============================
    *
    * Admin Styles
    *
    * @access public
    *
    ============================= */

    public function admin_styles() {

        global $post, $pagenow;

        // maybe limit this to particular pages, you can use:
        // if( $post && (get_post_type( $post->ID ) == "product" && ($pagenow == "post.php" || $pagenow == "post-new.php")) ){

        wp_register_style( $this->slug.'_admin_styles', $this->plugin_url . 'assets/admin/css/main.min.css', array(), $this->version );
        wp_enqueue_style( $this->slug.'_admin_styles' );
        wp_enqueue_style('font-awesome', $this->plugin_url . 'assets/font-awesome/css/font-awesome.min.css'); 
        wp_enqueue_style('jquery-ui', $this->plugin_url . 'assets/vendor/jquery-ui/jquery-ui.min.css'); 

    }

/** =============================
    *
    * Admin Scripts
    *
    * @access public
    *
    ============================= */

    public function admin_scripts() {

        global $post, $pagenow;

        wp_register_script( $this->slug.'_admin_scripts', $this->plugin_url . 'assets/admin/js/main.min.js', array( 'jquery' ), $this->version, true);
        wp_enqueue_script( $this->slug.'_admin_scripts' );
        wp_enqueue_script('jquery-ui', $this->plugin_url . 'assets/vendor/jquery-ui/jquery-ui.min.js');

    }

/** =============================
    *
    * Set up Deliveries DB
    *
    * Used to store the status of individual parts of each order
    *
    ============================= */

    public function setup_deliveries_db() {

        global $wpdb;

        $charset_collate = $wpdb->get_charset_collate();
        $installed_ver = get_option( $this->deliveries_table_slug."_db_version" );

        if( $installed_ver != $this->db_version ) {

            $sql = "CREATE TABLE `{$wpdb->prefix}{$this->deliveries_table_slug}` (
            `id` mediumint(9) NOT NULL AUTO_INCREMENT,
            `order_id` mediumint(9),
            `delivery_id` mediumint(9),
            `status` text,
            `tracking` text,
            UNIQUE KEY id (id)
            ) $charset_collate;";

            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            $return = dbDelta( $sql );

            update_option( $this->deliveries_table_slug."_db_version", $this->db_version );

        }

    }

/** =============================
    *
    * Ajax: Add delivery flagging
    *
    * @param int $order_id
    *
    ============================= */

    public function flag_delivery() {

        global $wpdb, $sitepress;

        $lang = isset( $_POST['lang'] ) ? $_POST['lang'] : "en";

        $sitepress->switch_lang($lang, true);
        $this->textdomain();

        header('Content-Type: application/json');

        $return = array();
        $return['success'] = false;

        $order_id = ( isset( $_POST['order_id'] ) && $_POST['order_id'] != "" ) ? $_POST['order_id'] : false;
        $delivery_id = ( isset( $_POST['delivery_id'] ) && $_POST['delivery_id'] != "" ) ? $_POST['delivery_id'] : false;

        if ($order_id !== false && $delivery_id !== false){
            
            // One order may contain multiple deliveries so we need to build an array of delivery ids
            // that can be stored and checked against the order.
            // If there are already delivery_ids set for the order then we'll want to update the ids that are flagged
            $delivery_ids_meta = get_post_meta($order_id, 'flagged', true);
            $delivery_ids = $delivery_ids_meta;

            if (isset($delivery_ids) && $delivery_ids){
                // We will only update the post meta though if the id doesn't already exist in the array
                if (!in_array($delivery_id, $delivery_ids)){

                    if (is_array($delivery_ids)){
                        $delivery_ids[] = $delivery_id;
                    } else {
                        $delivery_ids = array();
                        $delivery_ids[] = $delivery_id;
                    }
                    update_post_meta($order_id, 'flagged', $delivery_ids);
                }
            } else {
                // If the order doesn't have any deliveries marked as flagged then we can create the post meta
                add_post_meta($order_id, 'flagged', [$delivery_id], true);
            }

            // delete_post_meta($order_id, 'flagged');
            // add_post_meta($order_id, 'flagged', true, true);
            $return['success'] = true;
        }

        echo json_encode($return);

        wp_die();

    }

/** =============================
    *
    * Ajax: get a flagged delivery
    *
    * @param int $order_id
    *
    ============================= */

    public function unflag_delivery() {

        global $wpdb, $sitepress;

        $lang = isset( $_POST['lang'] ) ? $_POST['lang'] : "en";

        $sitepress->switch_lang($lang, true);
        $this->textdomain();

        header('Content-Type: application/json');

        $return = array();
        $return['success'] = false;

        $order_id = ( isset( $_POST['order_id'] ) && $_POST['order_id'] != "" ) ? $_POST['order_id'] : false;
        $delivery_id = ( isset( $_POST['delivery_id'] ) && $_POST['delivery_id'] != "" ) ? $_POST['delivery_id'] : false;


        if ($order_id !== false && $delivery_id !== false){
            
            // We need to check to see if the order has data before removing the delivery
            if ($delivery_ids = get_post_meta($order_id, 'flagged', true)){
                // Check to see if the delivery_id exists within the array

                if (in_array($delivery_id, $delivery_ids)){
                    $key = array_search($delivery_id, $delivery_ids);
                    unset($delivery_ids[$key]);

                    if (count($delivery_ids) === 0){
                        // If we have removed all deliveries from the delivery_ids then we can remove the flag entirely
                        delete_post_meta($order_id, 'flagged');
                    } else {
                        // If there are still other packages in the delivery then we should update
                        // the list of packages that've already been flagged
                        update_post_meta($order_id, 'flagged', $delivery_ids);
                    }
                }
            } else {

                // If there are no delivery id's and the unflag endpoint has been called
                // we should go ahead and remove the flag from post meta
                delete_post_meta($order_id, 'flagged');
            }
            
            $return['success'] = true;
        }

        echo json_encode($return);

        wp_die();

    }

/** =============================
    *
    * Ajax: Add delivery completion
    *
    * @param int $order_id
    * @param int $delivery_id
    *
    ============================= */

    public function add_delivery_completion() {

        global $wpdb, $sitepress;

        $lang = isset( $_POST['lang'] ) ? $_POST['lang'] : "en";

        $sitepress->switch_lang($lang, true);
        $this->textdomain();

        header('Content-Type: application/json');

        $return = array();
        $return['success'] = false;
        $order_id = ( isset( $_POST['order_id'] ) && $_POST['order_id'] != "" ) ? $_POST['order_id'] : false;
        $delivery_id = ( isset( $_POST['delivery_id'] ) && $_POST['delivery_id'] != "" ) ? $_POST['delivery_id'] : false;
        $delivery_type = ( isset( $_POST['delivery_type'] ) && $_POST['delivery_type'] != "" ) ? $_POST['delivery_type'] : "Postal";
        $order_parts = ( isset( $_POST['order_parts'] ) && $_POST['order_parts'] != "" ) ? $_POST['order_parts'] : false;
        $recipient_name = ( isset( $_POST['recipient_name'] ) && $_POST['recipient_name'] != "" ) ? $_POST['recipient_name'] : "";
        $recipient_address = ( isset( $_POST['recipient_address'] ) && $_POST['recipient_address'] != "" ) ? $_POST['recipient_address'] : "";
        $recipient_flowers = ( isset( $_POST['recipient_flowers'] ) && $_POST['recipient_flowers'] != "" ) ? $_POST['recipient_flowers'] : "";
        $customer_email = ( isset( $_POST['customer_email'] ) && $_POST['customer_email'] != "" ) ? $_POST['customer_email'] : false;
        $tracking_number = ( isset( $_POST['tracking_number'] ) && $_POST['tracking_number'] != "" ) ? $_POST['tracking_number'] : false;
        $delivery_date = ( isset( $_POST['delivery_date'] ) && $tracking_number ) ? $_POST['delivery_date'] : "";

        if( $order_id !== false && $delivery_id !== false ) {

            if( $this->delivery_exists( $order_id, $delivery_id ) ) {

                $insert = $wpdb->update(
                    $this->deliveries_table_name,
                    array(
                        'status' => "completed",
                    ),
                    array(
                        'order_id' => $order_id,
                        'delivery_id' => $delivery_id
                    ),
                    array(
                        '%s'
                    ),
                    array(
                        '%d',
                        '%d'
                    )
                );

            } else {

                $insert = $wpdb->insert(
                    $this->deliveries_table_name,
                    array(
                        'order_id' => $order_id,
                        'delivery_id' => $delivery_id,
                        'status' => "completed",
                        'tracking' => $tracking_number
                    ),
                    array(
                        '%d',
                        '%d',
                        '%s',
                        '%s'
                    )
                );

            }

            if( $insert ) {
                $return['success'] = true;

                $final_part = $this->is_final_order_part( $order_id, $order_parts );

                if( $final_part ) {

                    // Set Order status

                    $the_order = array();
                    $the_order['ID'] = $order_id;
                    $the_order['post_status'] = 'wc-completed';
                    wp_update_post( $the_order );

                }

                // Send customer email

                if( $customer_email ) {

                    $subject = $this->wpml_translate_words('Your Panamy Flower Order', 'pltfrm-panamy-deliveries', $lang).' #'.$order_id;

                    if( $tracking_number ) {

                        $message = sprintf( $this->wpml_translate_words('Thank you for your recent order. Your bouquet "<strong>%s</strong>" has left our atelier and is now on the way to <strong>%s</strong> and will be delivered on <strong>%s</strong> at the following address: <br><br>%s', 'pltfrm-panamy-deliveries', $lang), $recipient_flowers, $recipient_name, $delivery_date, $recipient_address );

                        $message .= sprintf( $this->wpml_translate_words('<br><br>Click the link to <a href="https://service.post.ch/EasyTrack/submitParcelData.do?formattedParcelCodes=99.60.074025.%s">track your order</a>.', 'pltfrm-panamy-deliveries', $lang), $tracking_number );

                    } else {

                        $message = sprintf( $this->wpml_translate_words('Thank you for your recent order. Your bouquet "<strong>%s</strong>" has left our atelier and is now on the way to <strong>%s</strong>. It will be delivered today at the following address: <br><br>%s', 'pltfrm-panamy-deliveries', $lang), $recipient_flowers, $recipient_name, $recipient_address );

                    }

                    $message .= '<br><br><img src="'.$this->plugin_url.'assets/frontend/img/email.jpg" style="height: auto; width: 100%; max-width: 100%; display: block;" />';

                    $mailer = WC()->mailer();
                    $message = $mailer->wrap_message( $subject, $message );

                    $mailer->send( $customer_email, $subject, $message );

                }

                // Remove the delivery is_flagged post meta
                // delete_post_meta($order_id, 'is_flagged');

            } else {
                $return['success'] = false;
            }

        }

        $return['post'] = $_POST;

        echo json_encode( $return );

        wp_die();

    }

/** =============================
    *
    * Helper: Is final order part
    *
    * @param  [int] [$order_id]
    * @param  [int] [$parts]
    * @return [bool]
    *
    ============================= */

    public function is_final_order_part( $order_id, $parts ) {

        global $wpdb;

        $complete_count = $wpdb->get_var( "SELECT COUNT(*) FROM $this->deliveries_table_name WHERE order_id = '$order_id' AND status = 'completed'" );

        return $complete_count == $parts ? true : false;

    }

/** =============================
    *
    * Helper: Is delivery complete
    *
    * @param  [int] [$order_id]
    * @param  [int] [$delivery_id]
    * @return [bool]
    *
    ============================= */

    public function is_delivery_complete( $order_id, $delivery_id ) {

        global $wpdb;

        $complete = $wpdb->get_row( "SELECT * FROM $this->deliveries_table_name WHERE order_id = '$order_id' AND delivery_id = '$delivery_id' AND status = 'completed'" );

        return $complete ? true : false;

    }

/** =============================
    *
    * Helper: Delivery exists
    *
    * @param  [int] [$order_id]
    * @param  [int] [$delivery_id]
    * @return [bool]
    *
    ============================= */

    public function delivery_exists( $order_id, $delivery_id ) {

        global $wpdb;

        $delivery = $wpdb->get_row( "SELECT * FROM $this->deliveries_table_name WHERE order_id = '$order_id' AND delivery_id = '$delivery_id'" );

        return $delivery ? true : false;

    }

/** =============================
    *
    * Deliveries admin page
    *
    ============================= */

    public function deliveries_page() {

        if( current_user_can('deliveries-manager') ) {

            add_menu_page( $this->shortname, $this->shortname, 'manage_deliveries', $this->slug, array( $this, 'deliveries_page_display' ), "", 20 );

        } else {

            add_submenu_page( 'woocommerce', $this->shortname, $this->shortname, 'manage_deliveries', $this->slug, array( $this, 'deliveries_page_display' ) );

        }

    }

    public function deliveries_page_display() {

        if ( !current_user_can( 'manage_deliveries' ) )  {
            wp_die( __( 'You do not have sufficient permissions to access this page.', 'pltfrm-panamy-deliveries' ) );
        }

        require_once('inc/admin/deliveries.php');

    }

/** =============================
    *
    * Get all deliveries
    *
    * @param str $type all/upcoming/past
    * @param str $order asc/desc
    * @param str $order_by packing_date/delivery_date
    *
    ============================= */

    public function get_deliveries($package_status = "processing", $filter_packing_date_start = "", $filter_packing_date_end = "", $order = "asc", $order_by = "packing_date" ) {

        global $woocommerce;

        // The $order value passed to this method conflicts with the variable that gets set in the have_posts loop
        // therefore we're going to define a new variable called $sort_direction to handle column sorting
        $sort_direction = $order;

        $deliveries = array();
        $order_items = array();
        $delivery_ids = array();
        $delivery_i = 0;

        $args = array(
            'post_type' => 'shop_order',
            'posts_per_page' => -1,
            'post_status' => ['wc-processing'], // Get both status' to allow filtering of past orders,
        );

        // If the filter requests complete orders we need to include the wc-completed orders to allow us to loop over the order packages
        if ($package_status == 'complete'){
            $args['post_status'] = ['wc-processing', 'wc-completed'];
        }

        // If the filter packing date is set then we should apply a date query, we'll add 2 months either side of the requested date as 
        // a range to reduce the number of posts returned
        if ($filter_packing_date_start != '' && $filter_packing_date_end != ''){
            $filter_tolerance = 6;
            $after  = explode('-', date('Y-m-d', strtotime("$filter_packing_date_start -$filter_tolerance month")));
            $before  = explode('-', date('Y-m-d', strtotime("$filter_packing_date_end")));
            $args['date_query'] = [
                [
                    'after' => [
                        'year' => $after[0],
                        'month' => $after[1],
                        'day' => $after[2]
                    ],
                    'before' => [
                        'year' => $before[0],
                        'month' => $before[1],
                        'day' => $before[2]
                    ],
                    'inclusive' => true
                ]
            ];
        }

        $orders = new WP_Query( $args );

        if ( $orders->have_posts() ) {

            while ( $orders->have_posts() ) {
                $orders->the_post();
                $order = new WC_Order( get_the_id() );
                $order_id = get_the_id();
                $order_items[$order_id] = 0;
                $delivery_ids[$order_id] = 0;

                $lang = get_post_meta($order_id, 'lang', true);

                $customer = $order->get_user();

                $customer_name = array();
                $customer_name[] = $order->billing_first_name;
                $customer_name[] = $order->billing_last_name;


                $packages = $this->get_delivery_packages( $order );              

                // Get all of the flagged delivery ids
                $flagged_delivery_ids = get_post_meta($order_id, 'flagged', true);

                if( $packages && !empty( $packages ) ) {

                    foreach( $packages as $package ) {

                        $order_type = $this->get_delivery_type($package['full_address']['postcode']);

                        foreach( $package['contents'] as $order_item ) {

                            $todays_date = date('Y-m-d h:i:s');
                            $delivery_date_str = $order_item['delivery_date'];
                            $delivery_date = date_create_from_format( 'd/m/Y', $delivery_date_str );
                            $packing_date = date_create_from_format( 'd/m/Y', $delivery_date_str );
                            $order_date = date_create_from_format( 'Y-m-d H:i:s', $order->order_date );

                            $order_items[$order_id] += $order_item['qty'];

                            if( $order_type == "Postal" ) $packing_date->sub(new DateInterval('P1D'));

                            unset($package['destination']['first_name'],$package['destination']['last_name']);

                            for( $i = 0; $i < $order_item['qty']; $i++ ) {

                                // We need to check what status of the order currently 
                                // is to decide if it should be included in the list or not
                                // this is essentially a filter for the packages
                                $list_package = true;
                                if ($package_status == 'processing'){
                                    $list_package = (!$this->is_delivery_complete( $order_id, $delivery_ids[$order_id] ) ? true : false);
                                } else if ($package_status == 'complete'){
                                    $list_package = ($this->is_delivery_complete( $order_id, $delivery_ids[$order_id] ) ? true : false);
                                }

                                // // If the packing date filter is set check this against the package packing date
                                if ($filter_packing_date_start != '' && $filter_packing_date_end != ''){
                                    // If the package is already set to true then we should check the packing date filter between the packing
                                    // date start and end range
                                    if ($list_package){
                                        $packing_date_str_time = strtotime($packing_date->format('Y-m-d'));
                                        $filter_packing_date_start_str_time = strtotime($filter_packing_date_start);
                                        $filter_packing_date_end_str_time = strtotime($filter_packing_date_end);
                                        if ($packing_date_str_time >= $filter_packing_date_start_str_time && $packing_date_str_time <= $filter_packing_date_end_str_time ){
                                            $list_package = true;
                                        } else {
                                            $list_package = false;
                                        }
                                    }
                                }

                                // Check if we should list the package or not
                                if ($list_package){
                                    $package_number = $delivery_ids[$order_id] + 1;
                                    $total_packages_in_order = count($packages);

                                    if( $order_by == "packing_date" ) {

                                        $delivery_group_key = $packing_date->format( 'Ymd' );
                                        $delivery_key = $order_date->format('Ymdhis') .'-'. $total_packages_in_order - $package_number; // We minus count the package number down from the total packages to force the reversed order

                                    } elseif( $order_by == "delivery_date" ) {

                                        $delivery_group_key = $delivery_date->format( 'Ymd' );
                                        $delivery_key = $order_date->format('Ymdhis') .'-'. $total_packages_in_order - $package_number;

                                    }

                                    /**
                                     * Strip the "Variation #XXX of" text from name if it exists
                                     */
                                    $new_product_name = $order_item['product_name'];
                                    $split_product_name = explode(" ", $order_item['product_name']);

                                    if ($split_product_name[0] == "Variation") {
                                        $new_product_name = array_slice($split_product_name, 3);
                                        $new_product_name = implode(" ", $new_product_name);
                                    }

                                    $delivery_key = $this->format_key( $delivery_key, $deliveries[$delivery_group_key] );

                                    $deliveries[$delivery_group_key][$delivery_key]['order_id'] = $order_id;
                                    $deliveries[$delivery_group_key][$delivery_key]['delivery_id'] = $delivery_ids[$order_id];
                                    $deliveries[$delivery_group_key][$delivery_key]['order_date'] = $order_date;
                                    $deliveries[$delivery_group_key][$delivery_key]['order_status'] = $order->get_status();

                                    $shipping_method_options = panamy_get_shipping_method_options($order->get_shipping_method());
                                    $deliveries[$delivery_group_key][$delivery_key]['shipping_method'] = ($shipping_method_options ? $shipping_method_options['name'] : 'Express');

                                    $deliveries[$delivery_group_key][$delivery_key]['customer_name'] = implode(' ',$customer_name);
                                    $deliveries[$delivery_group_key][$delivery_key]['customer_email'] = $order->billing_email;
                                    $deliveries[$delivery_group_key][$delivery_key]['customer_phone'] = $order->billing_phone;
                                    $deliveries[$delivery_group_key][$delivery_key]['product_name'] = $new_product_name;
                                    $deliveries[$delivery_group_key][$delivery_key]['variation_details'] = $order_item['variation_details'];
                                    $deliveries[$delivery_group_key][$delivery_key]['delivery_type'] = $order_type;
                                    $deliveries[$delivery_group_key][$delivery_key]['packing_date'] = $packing_date;
                                    $deliveries[$delivery_group_key][$delivery_key]['delivery_date'] = $delivery_date;
                                    $deliveries[$delivery_group_key][$delivery_key]['packing_date_formatted'] = $packing_date->format( 'd/m/Y' );
                                    $deliveries[$delivery_group_key][$delivery_key]['delivery_date_formatted'] = $delivery_date->format( 'd/m/Y' );
                                    $deliveries[$delivery_group_key][$delivery_key]['address'] = $woocommerce->countries->get_formatted_address( $package['full_address'] );
                                    $deliveries[$delivery_group_key][$delivery_key]['recipient_name'] = $order_item['recipient_name'];
                                    $deliveries[$delivery_group_key][$delivery_key]['recipient_phone'] = isset( $package['destination']['phone'] ) ? $package['destination']['phone'] : "";
                                    $deliveries[$delivery_group_key][$delivery_key]['recipient_door_code'] = isset( $package['destination']['door_code'] ) ? $package['destination']['door_code'] : "";
                                    $deliveries[$delivery_group_key][$delivery_key]['message'] = $order_item['message'];
                                    $deliveries[$delivery_group_key][$delivery_key]['lang'] = $lang;

                                    $b2b_roles = (function_exists('get_field') ? explode(',', get_field('panamy_b2b_roles', 'option')) : ['customer-b2b']);
                                    $deliveries[$delivery_group_key][$delivery_key]['customer_account'] = $customer;
                                    $deliveries[$delivery_group_key][$delivery_key]['customer_account_is_b2b'] = ($customer ? ( count(array_intersect($customer->roles, $b2b_roles)) > 0 ? true : false ) : false);
                                    if ($deliveries[$delivery_group_key][$delivery_key]['customer_account_is_b2b']){
                                        $b2b_label_colour = (function_exists('get_field') ? ( !get_field('panamy_b2b_label_colour', 'user_'.$customer->ID) ? '#f1bb3b' : get_field('panamy_b2b_label_colour', 'user_'.$customer->ID) ) : '#f1bb3b');
                                        $deliveries[$delivery_group_key][$delivery_key]['customer_account_b2b_label_colour'] = $b2b_label_colour;
                                    }

                                    $deliveries[$delivery_group_key][$delivery_key]['package_number'] = $package_number;
                                    $deliveries[$delivery_group_key][$delivery_key]['packages_in_order'] = $total_packages_in_order;
                                    $deliveries[$delivery_group_key][$delivery_key]['is_flagged'] = (is_array($flagged_delivery_ids) ? in_array($deliveries[$delivery_group_key][$delivery_key]['delivery_id'], $flagged_delivery_ids) : false );
                                    $deliveries[$delivery_group_key][$delivery_key]['is_complete'] = $this->is_delivery_complete( $order_id, $delivery_ids[$order_id] );
                                }

                                $delivery_ids[$order_id]++;

                            }

                        }

                    }

                }

            }

        }

        wp_reset_postdata();

        // Sort deliveries by key
        ksort($deliveries);

        // Once we've finished adding all of the deliveres we can now sort each 
        // group (day) by the time the order was placed to ensure the latest
        // deliveries always appear on  and in delivery_id order ascending
        foreach ($deliveries as $delivery_group_key => $delivery_group){
            krsort($deliveries[$delivery_group_key]);
        }

        if( $sort_direction == "desc" ) $deliveries = array_reverse($deliveries);

        // Now sort by is_flagged to ensure delivereies not printed in order are not missed
        foreach ($deliveries as $delivery_group_key => &$delivery_group){
            $flagged = [];
            $not_flagged = [];
            foreach ($delivery_group as $delivery){
                if ($delivery['is_flagged']){
                    $flagged[] = $delivery;
                } else {
                    $not_flagged[] = $delivery;
                }
            }
            $delivery_group = array_merge($not_flagged, $flagged);
        }

        return array(
            'deliveries' => $deliveries,
            'order_items' => $order_items
        );

    }

    /**
     * Helper: Get delivery packages
     *
     * @param obj $order
     * @return array
     */
    public function get_delivery_packages( $order ) {

        $packages = get_post_meta( $order->id, '_wcms_packages', true );
        $formatted_packages = array();

        if( $packages ) {

            foreach( $packages as $package ) {

                $contents = array();
                $order_items = $package['contents'];

                // loop items to build condensed contents array
                if( $order_items && !empty( $order_items ) ) {

                    foreach( $order_items as $item_id => $order_item ) {
                        
                        if( isset( $order_item['tmcartepo'][0]['value'] ) ) {

                            $contents[$item_id] = array(
                                'product_name' => get_post($order_item['product_id'])->post_title,
                                'delivery_date' => $order_item['tmcartepo'][0]['value'],
                                'recipient_name' => $order_item['tmcartepo'][1]['value'],
                                'message' => $order_item['tmcartepo'][2]['value'],
                                'qty' => (int)$order_item['quantity'],
                                'variation_details' => ( $order_item['variation'] && !empty($order_item['variation']) ) ? wc_get_formatted_variation( $order_item['variation'], true , false) : false
                            );

                        }

                    }

                }

                if( !empty( $contents ) ) {

                    // add to formatted packages
                    $formatted_packages[] = array(
                        'contents' => $contents,
                        'destination' => $package['destination'],
                        'full_address' => ($package['full_address'] ? $package['full_address'] : $package['destination']),
                    );

                }

            }

        } else {

            $contents = array();
            $order_items = $order->get_items();

            // loop items to build condensed contents array
            if( $order_items && !empty( $order_items ) ) {

                foreach( $order_items as $item_id => $order_item ) {

                    if( isset( $order_item['tmcartepo_data'] ) ) {

                        $tmcartepo = maybe_unserialize( $order_item['tmcartepo_data'] );

                        if( $order_item['variation_id'] > 0 ) {
                            $variation = new WC_Product_Variation( $order_item['variation_id'] );
                            $variation_data = $variation->get_variation_attributes();
                            $variation_detail = wc_get_formatted_variation( $variation_data, true , false);
                        }

                        $contents[$item_id] = array(
                            'product_name' => $order_item['name'],
                            'delivery_date' => $tmcartepo[0]['value'],
                            'recipient_name' => $tmcartepo[1]['value'],
                            'message' => $tmcartepo[2]['value'],
                            'qty' => absint( $order_item['qty'] ),
                            'variation_details' => $order_item['variation_id'] > 0 ? $variation_detail : false
                        );

                    }

                }

            }

            if( !empty( $contents ) ) {

                $shipping_address = array(
                    'first_name' => $order->shipping_first_name,
                    'last_name' => $order->shipping_last_name,
                    'company' => $order->shipping_company,
                    'country' => $order->shipping_country,
                    'address_1' => $order->shipping_address_1,
                    'address_2' => $order->shipping_address_2,
                    'city' => $order->shipping_city,
                    'state' => $order->shipping_state,
                    'postcode' => $order->shipping_postcode,
                    'phone' => $order->shipping_phone,
                    'door_code' => $order->shipping_door_code
                );

                // add to formatted packages
                $formatted_packages[] = array(
                    'contents' => $contents,
                    'destination' => $shipping_address,
                    'full_address' => $shipping_address
                );

            }

        }

        return $formatted_packages;

    }

/** =============================
    *
    * Format Array Key
    *
    * @param int $key
    * @param arr $array
    * @return int Formatted Key
    *
    ============================= */

    public function format_key( $key, $array ) {

        $key = (int)$key;

        for( $i = $key; $i < $key+10000; $i++ ) {

            if( !isset( $array[$i] ) ) {

                return $i;

            }

        }

    }

/** =============================
    *
    * Get delivery type
    *
    * @param str $postcode
    * @return str Delivery type Local/Postal
    *
    ============================= */

    public function get_delivery_type( $postcode = false ){

        if( !$postcode )
            return "Local";

        $local_postcodes = apply_filters( 'panamy_local_postcodes', array(
            '1248', '1247', '1246', '1245', '1254', '1251', '1252', '1222', '1244', '1243', '1253', '1241', '1223', '1224', '1225', '1226', '1207', '1206', '1208', '1204', '1205', '1201', '1231', '1255', '1234', '1256', '1227', '1228', '1213', '1212', '1257', '1258', '1232', '1219', '1203', '1202', '1209', '1220', '1216', '1214', '1242', '1283', '1281', '1288', '1233', '1236', '1237', '1287', '1284', '1285', '1286', '1217', '1215', '1218', '1292', '1293', '1294', '1239', '1290', '1295', '1291', '1296', '1297', '1279', '1298'
        ) );

        if( in_array($postcode, $local_postcodes) ) {
            return "Local";
        } else {
            return "Postal";
        }

    }

/** =============================
    *
    * Allow to remove method for a hook when it's a class method used
    * and the class doesn't have a variable assigned, but the class name is known
    *
    * @hook_name:     Name of the wordpress hook
    * @class_name:    Name of the class where the add_action resides
    * @method_name:   Name of the method to unhook
    * @priority:      The priority of which the above method has in the add_action
    *
    ============================= */

    public function remove_filters_for_anonymous_class( $hook_name = '', $class_name ='', $method_name = '', $priority = 0 ) {

        global $wp_filter;

        // Take only filters on right hook name and priority
        if ( !isset($wp_filter[$hook_name][$priority]) || !is_array($wp_filter[$hook_name][$priority]) )
                return false;

        // Loop on filters registered
        foreach( (array) $wp_filter[$hook_name][$priority] as $unique_id => $filter_array ) {
                // Test if filter is an array ! (always for class/method)
                if ( isset($filter_array['function']) && is_array($filter_array['function']) ) {
                        // Test if object is a class, class and method is equal to param !
                        if ( is_object($filter_array['function'][0]) && get_class($filter_array['function'][0]) && get_class($filter_array['function'][0]) == $class_name && $filter_array['function'][1] == $method_name ) {
                                unset($wp_filter[$hook_name][$priority][$unique_id]);
                        }
                }

        }

        return false;

    }

    /**
     * Prevent admin access unless user can manage deliveries
     */
    public function prevent_admin_access( $prevent_access ) {

       if ( current_user_can('manage_deliveries') ) {
         $prevent_access = false;
       }

       return $prevent_access;

    }

    /**
     * Change delivery status on order status change
     *
     * @param int $order_id
     * @param str $old_status
     * @param str $new_status
     */
    public function change_delivery_status( $order_id, $old_status, $new_status ) {

        global $wpdb;

        $wpdb->update(
            $this->deliveries_table_name,
            array( 'status' => $new_status ),
            array( 'order_id' => $order_id ),
            array( '%s' ),
            array( '%d' )
        );

    }

/** =============================
    *
    * Get Woo Version Number
    *
    * @return mixed bool/str NULL or Woo version number
    *
    ============================= */

    public function get_woo_version_number() {

        // If get_plugins() isn't available, require it
        if ( ! function_exists( 'get_plugins' ) )
            require_once( ABSPATH . 'wp-admin/includes/plugin.php' );

            // Create the plugins folder and file variables
        $plugin_folder = get_plugins( '/' . 'woocommerce' );
        $plugin_file = 'woocommerce.php';

        // If the plugin version number is set, return it
        if ( isset( $plugin_folder[$plugin_file]['Version'] ) ) {
            return $plugin_folder[$plugin_file]['Version'];

        } else {
        // Otherwise return null
            return NULL;
        }

    }

    /**
     * Run when a new order is placed
     *
     * @param int $order_id
     */
    public function process_new_order( $order_id ) {

        add_post_meta($order_id, 'lang', ICL_LANGUAGE_CODE);

    }

    /**
     * Force WPML translation
     */
    public function wpml_translate_words($text, $domain, $lang) {

        global $wpdb;

        $string = $wpdb->get_var("SELECT value FROM {$wpdb->prefix}icl_string_translations WHERE language='".$lang."' AND string_id in (SELECT id from {$wpdb->prefix}icl_strings WHERE context LIKE '%".$domain."%' AND value LIKE '%".$text."%')");

        if( !$string )
            return $text;

        return $string;

    }

}

$pltfrm_panamy_deliveries = new Pltfrm_Panamy_Deliveries();
