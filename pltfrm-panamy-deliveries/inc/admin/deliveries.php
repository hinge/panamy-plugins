<?php

    global $the_order; 

    $packing_date_start = (isset($_GET['packing_date_start']) ? $_GET['packing_date_start'] : '' );
    $packing_date_end = (isset($_GET['packing_date_end']) ? $_GET['packing_date_end'] : '' );
    $package_status = (isset($_GET['package_status']) ? $_GET['package_status'] : 'processing');
    $sort_direction = ($_GET['sort_direction'] ? $_GET['sort_direction'] : 'asc');
    $delivery_data = $this->get_deliveries($package_status, $packing_date_start, $packing_date_end, $order = $sort_direction);
    
    // Debug deliveries data
    // echo "<pre>";
    // print_r($delivery_data['deliveries']);
    // echo "</pre>";
    
    // Add the ability for the administrator to sort the columns
    $sortable = '';
    if (current_user_can('administrator')){
         $sortable = '&sort_direction='.($sort_direction == 'asc' ? 'desc' : 'asc');
    }

?>

<div class="wrap">

    <h2 style="margin-bottom: 20px;"><?php echo $this->name; ?></h2>

    <div class="pltfrm-deliveries-wrap">

        <div class="deliveries-toolbar print-hide  <?= (array_key_exists('packing_date_start', $_GET) || array_key_exists('packing_date_end', $_GET) || array_key_exists('package_status', $_GET) ? 'show-filters' : ''); ?>">

            <div class="filter-deliveries">
                <form method="get" action="?page=pltfrm-panamy-deliveries">
                    <input type="hidden" name="page" value="pltfrm-panamy-deliveries" />

                    <div class="input">
                        <span class="label">Packing date range <a class="clear" href="#">clear</a></span>
                        <input readonly type="date" name="packing_date_start" placeholder="Packing date" value="<?= $packing_date_start; ?>" />
                        <input readonly type="date" name="packing_date_end" placeholder="Packing date" value="<?= $packing_date_end; ?>" />
                    </div>

                    <div class="input">
                        <span class="label">Status</span>
                        <select name="package_status">
                            <option <?= ($package_status == 'processing' ? 'selected="selected"' : '') ?> value="processing">Processing</option>
                            <option <?= ($package_status == 'complete' ? 'selected="selected"' : '') ?> value="complete">Complete</option>
                        </select>
                    </div>

                    <button class="button" type="submit"><i class="fa fa-filter" aria-hidden="true"></i> Filter</button>

                    <img class="filter-loading" src="<?php echo admin_url('/images/spinner-2x.gif'); ?>" />

                </form>

            </div>

            <div class="show-filters">
                <button class="button"><i class="fa fa-filter" aria-hidden="true"></i> Show delivery filters</button>
            </div>

            <div class="print-deliveries">
                <button class="button"><i class="fa fa-print" aria-hidden="true"></i> Print Deliveries</button>
            </div>

        </div>

        <table id="pltfrm-deliveries" class="widefat fixed" cellspacing="0">
        	<thead>
        		<tr>
        			<th width="80px">Order #</th>
        			<th class="align-center" width="175px">Package #</th>
        			<th>Product Name</th>
                    <th class="align-center" width="125px">Shipping Method</th>
        			<th class="align-center" width="125px">Delivery Type</th>
        			<th width="150px" class="align-center sorted <?= $sort_direction; ?>"><a href="?page=pltfrm-panamy-deliveries<?= $sortable; ?>"><span>Packing Date</span> <span class="sorting-indicator"></span></a></th>
        			<th class="print-hide" width="175px">Actions</th>
        		</tr>
        	</thead>
        	<tbody>

        	    <?php if( !empty( $delivery_data['deliveries'] ) ) { ?>

        	        <?php foreach( $delivery_data['deliveries'] as $delivery_group ) { ?>

                        <?php foreach( $delivery_group as $delivery ) { ?>


                            <?php 

                                // PDF Parameters
                                $breaks = array("<br />","<br>","<br/>");

                                $delivery['address'] = str_ireplace($breaks, "\r\n", $delivery['address']);
                                $delivery['message'] = str_ireplace($breaks, "\r\n", $delivery['message']);

                                $customer_account_company_name = ($delivery['customer_account_is_b2b'] ? get_user_meta( $delivery['customer_account']->id, 'billing_company', true) : false);

                                $params = array(
                                    'recipient_name' => $delivery['recipient_name'],
                                    'recipient_phone' => $delivery['recipient_phone'],
                                    'recipient_door_code' => $delivery['recipient_door_code'],
                                    'customer_phone' => $delivery['customer_phone'],
                                    'address' => $delivery['address'],
                                    'message' => $delivery['message'],
                                    'order_number' => $delivery['order_id'],
                                    'customer_name' => $delivery['customer_name'],
                                    'product_name' => $delivery['product_name'],
                                    'details' => $delivery['variation_details'],
                                    'shipping_method' => $delivery['shipping_method'],
                                    'delivery_type' => $delivery['delivery_type'],
                                    'customer_account_is_b2b' => $delivery['customer_account_is_b2b'],
                                    'customer_account_company_name' => $customer_account_company_name,
                                    'customer_account_b2b_label_colour' => $delivery['customer_account_b2b_label_colour']
                                );

                                $pdf_query = http_build_query($params);

                            ?>

                    		<tr class="
                                <?= ($delivery['is_flagged'] ? ' flagged' : ''); ?> 
                                <?= ($delivery['is_complete'] ? ' completed' : ''); ?>
                                <?= ($delivery['packing_date']->format('Y-m-d') < date('Y-m-d') && !$delivery['is_complete'] && !$delivery['is_flagged'] ? ' overdue' : ''); ?>
                                <?= ($delivery['packing_date']->format('Y-m-d') > date('Y-m-d') && !$delivery['is_complete'] && !$delivery['is_flagged'] ? ' future' : ''); ?>"
                                data-order-id="<?php echo esc_attr( $delivery['order_id'] ); ?>"
                                data-delivery-id="<?php echo esc_attr( $delivery['delivery_id'] ); ?>"
                                data-order-parts="<?php echo esc_attr( $delivery_data['order_items'][$delivery['order_id']] ); ?>"
                                data-shipping-method="<?php echo esc_attr( $delivery['shipping_method'] ); ?>"
                                data-delivery-type="<?php echo esc_attr( $delivery['delivery_type'] ); ?>"
                                data-recipient-name="<?php echo esc_attr( $delivery['recipient_name'] ); ?>"
                                data-recipient-address="<?php echo esc_attr( $delivery['address'] ); ?>"
                                data-recipient-flowers="<?php echo esc_attr( $delivery['product_name'] ); ?>"
                                data-customer-email="<?php echo esc_attr( $delivery['customer_email'] ); ?>"
                                data-delivery-date="<?php echo esc_attr( $delivery['delivery_date_formatted'] ); ?>"
                                data-is-flagged="<?php echo esc_attr( $delivery['is_flagged'] ); ?>"
                                data-lang="<?php echo esc_attr( $delivery['lang'] ); ?>">
                    			<td>
                                    <?php if (current_user_can('administrator')){ ?>
                                        <a class="button" href="<?php echo admin_url('post.php?action=edit&post='.$delivery['order_id']); ?>" target="_blank">#<?php echo $delivery['order_id']; ?></a>
                                    <?php } else { ?>
                                        <a class="button">#<?php echo $delivery['order_id']; ?></a> 
                                    <?php } ?>
                                </td>
                    			<td align="center">

                                    <?php if ($delivery['customer_account_is_b2b']){ ?>
                                        <div class="customer-b2b">
                                            <span class="b2b" style="background-color: <?= $delivery['customer_account_b2b_label_colour']; ?>;"><?= $customer_account_company_name ?> - B2B</span>
                                            <hr />
                                        </div>
                                    <?php } ?>

                                    <div class="package">
                                        <?php if ($delivery['packages_in_order'] > 1){ ?>
                                            Package <?= $delivery['package_number'] . ' of ' . $delivery['packages_in_order'] ?>
                                        <?php } else { ?>
                                            Single Package
                                        <?php } ?>
                                    </div>

                                    <div class="customer-b2b address">
                                        <hr /> 
                                        <?= $delivery['address']; ?>
                                    </div>

                                </td>
                    			<td>
                    			    <?php echo $delivery['product_name']; ?>
                    			    <?php if($delivery['variation_details']) echo '<br>'.$delivery['variation_details']; ?>
                                </td>
                                <td align="center"><span class="shipping-method shipping-method--<?php echo strtolower($delivery['shipping_method']); ?>"><?php echo $delivery['shipping_method']; ?></span></td>
                    			<td align="center"><span class="delivery-type delivery-type--<?php echo strtolower($delivery['delivery_type']); ?>"><?php echo $delivery['delivery_type']; ?></span></td>
                    			<td align="center"><?php echo $delivery['packing_date_formatted']; ?></td>
                    			<td class="print-hide">
                                    <div class="delivery-actions">

                                        <?php if (!$delivery['is_complete']){ ?>

                                            <a target="_blank" class="button print-labels pltfrm-print-labels" href="<?php echo $this->plugin_url; ?>inc/admin/print-label.php?print=1&<?php echo $pdf_query; ?>"><i class="fa fa-print" aria-hidden="true"></i> Print Label</a>

                                            <div class="delivery-tracking">
                                                <?php if( $delivery['delivery_type'] == "Postal" ) { ?>
                                			        <input class="tracking-number" type="text" placeholder="Tracking Number" value="">
                                			    <?php } ?>
                                			    <a class="button complete pltfrm-complete" href="<?php echo esc_attr( $delivery_key ); ?>"><i class="fa fa-check" aria-hidden="true"></i> Complete</a>
                                            </div>

                                            <?php if (current_user_can('administrator')){ ?>
                                                <div class="remove-flag">
                                                    <a class="pltfrm-remove-flag" href="#"><i class="fa fa-times-circle" aria-hidden="true"></i> Remove flag</a>
                                                </div>
                                            <?php } ?>

                                        <?php } else { ?>

                                            <span class="delivery-complete">Delivery Complete</span>

                                        <?php } ?>


                                    </div>
                                </td>

                    		</tr>

                        <?php } ?>

            		<?php } ?>

        		<?php } ?>

        	</tbody>
        </table>

        <div class="pltfrm-deliveries-loading"><img src="<?php echo admin_url('/images/spinner-2x.gif'); ?>" width="20" height="20"></div>

    </div>

</div>