<?php

if( isset($_GET['print']) ) {

    //============================================================+
    // File name   : example_001.php
    // Begin       : 2008-03-04
    // Last Update : 2013-05-14
    //
    // Description : Example 001 for TCPDF class
    //               Default Header and Footer
    //
    // Author: Nicola Asuni
    //
    // (c) Copyright:
    //               Nicola Asuni
    //               Tecnick.com LTD
    //               www.tecnick.com
    //               info@tecnick.com
    //============================================================+

    /**
     * Creates an example PDF TEST document using TCPDF
     * @package com.tecnick.tcpdf
     * @abstract TCPDF - Example: Default Header and Footer
     * @author Nicola Asuni
     * @since 2008-03-04
     */

    $border = 0; // 'LRTB';

    // Include the main TCPDF library (search for installation path).
    require_once('tcpdf/tcpdf.php');

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, "A5", true, 'UTF-8', false);

    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('James Kemp');
    $pdf->SetTitle('Delivery Label');
    $pdf->SetSubject('TCPDF Tutorial');
    $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

    $pdf->setPageUnit('mm');

    // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    // set margins
    $pdf->SetMargins(0, 0, 0);

    // set auto page breaks
    $pdf->SetAutoPageBreak(false);

    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }

    // ---------------------------------------------------------

    // set font
    $pdf->SetFont('Quickpen', 'BI', 18);

    // add a page
    $pdf->AddPage();

    // print message cell
    // 8 lines available, 3 space at top, 2 at bottom
    $message = nl2br(wordwrap( $_GET['message'], 50, "\n" ));
    $message_lines = count( explode(PHP_EOL, $message) );
    $message_padding = (8-$message_lines)/2;

    $message_output = "<br /><br /><br />"; // top padding always (printed logo)
    for ($x = 0; $x < floor($message_padding); $x++) {  $message_output .= "<br />"; } // top padding
    $message_output .= "<div>".$message."</div>";

    $pdf->writeHTMLCell(0, 105, '', '', $message_output, $border, 1, 0, true, 'C', true);

    // print order info cell
    $pdf->SetFont('helvetica', '', 6);
    $order_info = "<div>";

        $order_info .= "Order: #{$_GET['order_number']} | Customer: {$_GET['customer_name']} | Product: {$_GET['product_name']} | {$_GET['details']} <br />";
        $order_info .= "Delivery: {$_GET['delivery_type']} | T.S. {$_GET['customer_phone']} | T.R. {$_GET['recipient_phone']} | Door: {$_GET['recipient_door_code']}";

        if ($_GET['shipping_method'] == 'Express'){
            $order_info .= "<strong style=\" border: 0px solid #000000; color: #ffffff; background-color: #000000;\">&nbsp;".strtoupper($_GET['shipping_method'])."&nbsp;</strong>";
        } else {
            $order_info .= "<strong style=\" border: color: #000000;\">&nbsp;".strtoupper($_GET['shipping_method'])."&nbsp;</strong>";
        }

    $order_info .= "</div>";
    $pdf->writeHTMLCell(0, 5, 0, 107.5, $order_info, 0, 1, 0, true, 'C', true);

    // set font
    $pdf->SetFont('helvetica', '', 15);

    // print address cell
    // 11 lines available
    $address = "<div>".nl2br( $_GET['address'] )."</div>";
    $address_lines = count( explode(PHP_EOL, $address) );
    $address_padding = (8-$address_lines)/2;

    $address_output = "";
    for ($x = 0; $x < floor($address_padding); $x++) {  $address_output .= "<br />"; } // top padding
    $address_output .= $address;

    $pdf->writeHTMLCell(110, 50, 19.25, 115, $address_output, $border, 1, 0, true, 'C', true);

    // Show whether a customer order or not
    if ($_GET['customer_account_is_b2b']){
        $pdf->SetFont('helvetica', '', 8);
        $pdf->writeHTMLCell(30, 4.5, 23, 158.5, '<span style="display: block; color: white; background-color: '.$_GET['customer_account_b2b_label_colour'].';">&nbsp;'.$_GET['customer_account_company_name'].'&nbsp;</span>', $border, 1, 0, true, 'L', true);
    }

    // Order number on address label
    $pdf->SetFont('helvetica', '', 8);

    if ($_GET['shipping_method'] == 'Express'){
        $shipping_method = "<table width=\"100%\">".
                                "<tbody>".
                                    "<tr>". 
                                        "<td width=\"60%\" valign=\"middle\" align=\"left\">".
                                            "<table style=\"padding-top: 1px\">".
                                                "<tbody>".
                                                    "<tr>".
                                                        "<td align=\"left\">Order: #{$_GET['order_number']}</td>".
                                                    "</tr>".    
                                                "</tbody>".
                                            "</table>".
                                        "</td>".
                                        "<td width=\"40%\" valign=\"middle\">".
                                            "<table style=\" border: 0px solid #000000; color: #ffffff; background-color: #000000; padding-top: 2px\">".
                                                "<tbody>".
                                                    "<tr>".
                                                        "<td align=\"center\"><strong style=\"font-size: 8px;\">".strtoupper($_GET['shipping_method'])."</strong></td>".
                                                    "</tr>".    
                                                "</tbody>".
                                            "</table>".
                                        "</td>".
                                    "</tr>".
                                "</tbody>".
                            "</table>";
    } else {
        $shipping_method = "<table width=\"100%\">".
                                "<tbody>".
                                    "<tr>". 
                                        "<td width=\"60%\" valign=\"middle\" align=\"left\">".
                                            "<table style=\"padding-top: 1px\">".
                                                "<tbody>".
                                                    "<tr>".
                                                        "<td align=\"left\">Order: #{$_GET['order_number']}</td>".
                                                    "</tr>".    
                                                "</tbody>".
                                            "</table>".
                                        "</td>".
                                        "<td width=\"40%\" valign=\"middle\">".
                                            "<table style=\" border: 2px solid #000000; color: #000000; padding-top: 1px\">".
                                                "<tbody>".
                                                    "<tr>".
                                                        "<td align=\"center\"><strong style=\"font-size: 8px;\">".strtoupper($_GET['shipping_method'])."</strong></td>".
                                                    "</tr>".    
                                                "</tbody>".
                                            "</table>".
                                        "</td>".
                                    "</tr>".
                                "</tbody>".
                            "</table>";
    }

    $pdf->writeHTMLCell(40, 4.5, 92.25, 158.5, $shipping_method, $border, 1, 0, true, 'R', true);

    // print name cell
    // set font
    $pdf->SetFont('Quickpen', 'BI', 15);

    $name = "<div><br /><br />".$_GET['recipient_name']."</div>";

    $pdf->writeHTMLCell(110, 35, 19.25, 168, $name, $border, 1, 0, true, 'C', true);

    // ---------------------------------------------------------

    //Close and output PDF document
    $pdf->Output('example_002.pdf', 'I');

    //============================================================+
    // END OF FILE
    //============================================================+

}