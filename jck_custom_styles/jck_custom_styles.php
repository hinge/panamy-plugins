<?php /*
Plugin Name: Easy Custom Styles
Plugin URI: http://www.jckemp.com
Description: Easily add any number of custom text styles to the WYSIWYG editor.
Version: 1.0.2
Author: James Kemp
Author URI: http://www.jckemp.com
License: GPL2

Copyright 2014, James Kemp

*/

class jck_custom_styles
{
  function getValue($key, $default = '', $id = '')
  {
	if($id == '') {
		global $post;
		$jck_custom_styles = get_post_meta($post->ID, 'jck_custom_styles', true);
	} else {
		$jck_custom_styles = get_post_meta($id, 'jck_custom_styles', true);
	}
    if (isset($jck_custom_styles[$key])) {
      return $jck_custom_styles[$key];
    } //isset($jck_custom_styles[$key])
    else {
      return $default;
    }
  }
  
  function add_stylesheets()
  {
	global $post_type;
    
    if ('custom-styles' == $post_type) {
		wp_register_style('colorpicker_css', plugins_url('/assets/colorpicker/css/colorpicker.css', __FILE__));
		wp_enqueue_style('colorpicker_css');
		wp_register_style('jck_cs_styles', plugins_url('/assets/styles.css', __FILE__));
		wp_enqueue_style('jck_cs_styles');
	} //'custom-styles' == $post_type
  }
  
  // Enque admin scripts
  
  function add_admin_scripts()
  {
    global $post_type;
    
    if ('custom-styles' == $post_type) {
      wp_enqueue_script('colorpicker_script', plugins_url('/assets/colorpicker/js/colorpicker.js', __FILE__), 'jquery');
      wp_enqueue_script('scroll_script', plugins_url('/assets/scroll.js', __FILE__), 'colorpicker_script');
      wp_enqueue_script('custom_style_editor_scripts', plugins_url('/assets/scripts.js', __FILE__), 'scroll_script');
    } //'custom-styles' == $post_type
    
  }
	
  function getStyles() {
	global $wpdb;

	$jck_posts = $wpdb->get_results( 
	"	
		SELECT *	
		FROM $wpdb->posts	
		WHERE $wpdb->posts.post_type = 'custom-styles'	
		AND $wpdb->posts.post_status = 'publish'	
		ORDER BY ID DESC	
	"
	);
	
	return $jck_posts;
	$wpdb->flush();
  }
  
  /* Custom CSS styles on WYSIWYG Editor – Start
  ======================================= */
  
  function mce_buttons($buttons)
  {
	$jck_posts = $this->getStyles(); // Get All Custom Styles

    if (!in_array('styleselect', $buttons) && $jck_posts) {
      array_unshift($buttons, 'styleselect');
    } //!in_array('styleselect', $buttons)
    return $buttons;
  }
  
  // Add Styles to mce
  
  function mce_before_init($settings)
  {
    // Get existing styles
    if(isset($settings['style_formats'])) {
			$existing_styles = json_decode($settings['style_formats']);
		} else {
			$existing_styles = array();
		}
		
		$jck_posts = $this->getStyles(); // Get All Custom Styles
		
		if ( $jck_posts ) 
		{
			foreach ( $jck_posts as $jck_post ) 
			{ 
			  
				//$jck_custom_styles = get_post_meta($jck_post->ID, 'jck_custom_styles', true);
				
				$custom_style_settings = array(
					'title' => $jck_post->post_title,
					'attributes' => array(
						'class' => $jck_post->post_name
					),
					'wrapper' => false,
					'name' => $jck_post->post_name
				);
				
				if ($this->getValue('inline','',$jck_post->ID) == 'inline') {
					$custom_style_settings['inline'] = 'span';
				} //$this->getValue('inline') == 'inline'
				else {
					$custom_style_settings['block'] = 'p';
				}
				$style_formats[] = $custom_style_settings;
				
			} 
			
			// Merge existing styles with new ones.
      $merge = array_merge($style_formats, $existing_styles);
    	// Output merged styles as javascript   
      $settings['style_formats'] = json_encode($merge);
			
		} // End if posts
    
    return $settings;
    
  }
  
  
  // Add dynamic CSS
  
  function add_trigger($vars)
  {
    $vars[] = 'custom_styles_trigger';
    return $vars;
  }
  
  // Get the slug function
  
  function the_slug()
  {
    $post_data = get_post($post->ID, ARRAY_A);
    $slug      = $post_data['post_name'];
    return $slug;
  }
  
  function trigger_check()
  {
    if (intval(get_query_var('custom_styles_trigger')) == 1) {
			header("Content-type: text/css");
			
			
			$jck_posts = $this->getStyles(); // Get All Custom Styles
		
			if ( $jck_posts ) 
			{
				foreach ( $jck_posts as $jck_post ) 
				{ 
					
					$current_styles = $this->compile_styles(get_post_meta($jck_post->ID, 'jck_custom_styles', true),$jck_post->ID);        
					$slug = $jck_post->post_name;
					
					echo '.' . $slug . ' {' . $current_styles . '}' . "\n";
					
				}
				
			} // End if posts
 
      exit;
    } //intval(get_query_var('custom_styles_trigger')) == 1
  }
  
  // Add Stylesheet to frontend
  
  function add_dynamic_stylesheet()
  {
    $mce_css = get_bloginfo('url') . '?custom_styles_trigger=1';
    wp_register_style('custom_styles_css', $mce_css);
    wp_enqueue_style('custom_styles_css');
  }
  
  
  // Add Stylesheet to editor
  
  function plugin_mce_css($mce_css)
  {
    if (!empty($mce_css))
      $mce_css .= ',';
    $mce_css .= get_bloginfo('url') . '?custom_styles_trigger=1';
    return $mce_css;
  }
  
  
  /* =====
  Add Custom Post Type
  ===== */
  
  function post_type()
  {
    $labels = array(
      'name' => _x('Custom Styles', 'post type general name'),
      'singular_name' => _x('Custom Style', 'post type singular name'),
      'add_new' => _x('Add New', 'Custom Style'),
      'add_new_item' => __('Add New Custom Style'),
      'edit_item' => __('Edit Custom Style'),
      'new_item' => __('New Custom Style'),
      'view_item' => __('View Custom Stylea'),
      'search_items' => __('Search Custom Stylea'),
      'not_found' => __('No Custom Styles found'),
      'not_found_in_trash' => __('No Custom Styles found in Trash'),
      'parent_item_colon' => ''
    );
    
    register_post_type('custom-styles', array(
      'labels' => $labels,
      'public' => false,
      'show_ui' => true,
      'show_in_menu' => 'options-general.php',
      'supports' => array(
        'title'
      )
    ));
  }
  
  // Add meta box
  
  function metaboxes()
  {
    add_meta_box('style_selector', 'Style Selector', array(
      &$this,
      'custom_styles_meta'
    ), 'custom-styles', 'normal', 'high');
    
  }
  
  function custom_styles_meta()
  {
    global $post;
    $current_styles = $this->compile_styles(get_post_meta($post->ID, 'jck_custom_styles', true));
    
    wp_nonce_field('custom_styles_nonce', 'meta_box_nonce');
	
  	include('inc/admin-editor.php'); 

  }
  
  function save_custom_styles_meta($post_id)
  {
    // Bail if we're doing an auto save  
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
      return;
    
    // if our nonce isn't there, or we can't verify it, bail 
    if (!isset($_POST['meta_box_nonce']) || !wp_verify_nonce($_POST['meta_box_nonce'], 'custom_styles_nonce'))
      return;
    
    // if our current user can't edit this post, bail  
    if (!current_user_can('edit_posts'))
      return;
    
    // now we can actually save the data  
    
    // Make sure your data is set before trying to save it  
    if (isset($_POST['jck_custom_styles']))
      update_post_meta($post_id, 'jck_custom_styles', $_POST['jck_custom_styles']);
    
  }
  
  // =======================
  // Compile Saved Styles
  
  function compile_styles($jck_custom_styles, $id = '')
  {		
    $current_styles = '';
    
    if($this->getValue('fontFamily','',$id) != "inherit"){
        $current_styles .= ($this->getValue('fontFamily','',$id) != '') ? 'font-family:' . $this->getValue('fontFamily','',$id) . '; ' : '';
    }
    
    $current_styles .= ($this->getValue('fontSize','',$id) != '') ? 'font-size:' . $this->getValue('fontSize','',$id) . $this->getValue('fontSizeMeas','',$id) . '; ' : '';
    
    $current_styles .= ($this->getValue('color','',$id) != '') ? 'color:#' . $this->getValue('color','',$id) . '; ' : '';
    
    $current_styles .= ($this->getValue('fontWeight','',$id) != '') ? 'font-weight:' . $this->getValue('fontWeight','',$id) . '; ' : '';
    
    $current_styles .= ($this->getValue('fontStyle','',$id) != '') ? 'font-style:' . $this->getValue('fontStyle','',$id) . '; ' : '';
    
    $current_styles .= ($this->getValue('textDecoration','',$id) != '') ? 'text-decoration:' . $this->getValue('textDecoration','',$id) . '; ' : '';
	
	$current_styles .= ($this->getValue('textTransform','',$id) != '') ? 'text-transform:' . $this->getValue('textTransform','',$id) . '; ' : '';
    
    $current_styles .= ($this->getValue('textAlign','',$id) != '') ? 'text-align:' . $this->getValue('textAlign','',$id) . '; ' : '';
    
    $current_styles .= ($this->getValue('letterSpacing','',$id) != '') ? 'letter-spacing:' . $this->getValue('letterSpacing','',$id) . 'px; ' : '';
    
    $current_styles .= ($this->getValue('wordSpacing','',$id) != '') ? 'word-spacing:' . $this->getValue('wordSpacing','',$id) . 'px; ' : '';
    
    $current_styles .= ($this->getValue('lineHeight','',$id) != '') ? 'line-height:' . $this->getValue('lineHeight','',$id) . $this->getValue('lineHeightMeas','',$id) . '; ' : '';
    
    $current_styles .= ($this->getValue('backgroundColor','',$id) != '') ? 'background-color:#' . $this->getValue('backgroundColor','',$id) . '; ' : '';
    
    if ($this->getValue('margin_ind','',$id) == 'margin_ind') {
      $current_styles .= ($this->getValue('margin-top','',$id) != '') ? 'margin-top:' . $this->getValue('margin-top','',$id) . 'px; ' : '';
      $current_styles .= ($this->getValue('margin-right','',$id) != '') ? 'margin-right:' . $this->getValue('margin-right','',$id) . 'px; ' : '';
      $current_styles .= ($this->getValue('margin-bottom','',$id) != '') ? 'margin-bottom:' . $this->getValue('margin-bottom','',$id) . 'px; ' : '';
      $current_styles .= ($this->getValue('margin-left','',$id) != '') ? 'margin-left:' . $this->getValue('margin-left','',$id) . 'px; ' : '';
    } //$this->getValue('margin_ind','',$id) == 'margin_ind'
    else {
      $current_styles .= ($this->getValue('margin','',$id) != '') ? 'margin:' . $this->getValue('margin','',$id) . 'px; ' : '';
    }
    
    if ($this->getValue('padding_ind','',$id) == 'padding_ind') {
      $current_styles .= ($this->getValue('padding-top','',$id) != '') ? 'padding-top:' . $this->getValue('padding-top','',$id) . 'px; ' : '';
      $current_styles .= ($this->getValue('padding-right','',$id) != '') ? 'padding-right:' . $this->getValue('padding-right','',$id) . 'px; ' : '';
      $current_styles .= ($this->getValue('padding-bottom','',$id) != '') ? 'padding-bottom:' . $this->getValue('padding-bottom','',$id) . 'px; ' : '';
      $current_styles .= ($this->getValue('padding-left','',$id) != '') ? 'padding-left:' . $this->getValue('padding-left','',$id) . 'px; ' : '';
    } //$this->getValue('padding_ind','',$id) == 'padding_ind'
    else {
      $current_styles .= ($this->getValue('padding','',$id) != '') ? 'padding:' . $this->getValue('padding','',$id) . 'px; ' : '';
    }
    
    $current_styles .= ($this->getValue('borderStyle','',$id) != '') ? 'border-style:' . $this->getValue('borderStyle','',$id) . '; ' : '';
    
    $current_styles .= ($this->getValue('borderColor','',$id) != '') ? 'border-color:#' . $this->getValue('borderColor','',$id) . '; ' : '';
    
    if ($this->getValue('border_ind','',$id) == 'border_ind') {
      $current_styles .= ($this->getValue('border-top-width','',$id) != '') ? 'border-top-width:' . $this->getValue('border-top-width','',$id) . 'px; ' : 'border-top-width:0px; ';
      $current_styles .= ($this->getValue('border-right-width','',$id) != '') ? 'border-right-width:' . $this->getValue('border-right-width','',$id) . 'px; ' : 'border-right-width:0px; ';
      $current_styles .= ($this->getValue('border-bottom-width','',$id) != '') ? 'border-bottom-width:' . $this->getValue('border-bottom-width','',$id) . 'px; ' : 'border-bottom-width:0px; ';
      $current_styles .= ($this->getValue('border-left-width','',$id) != '') ? 'border-left-width:' . $this->getValue('border-left-width','',$id) . 'px; ' : 'border-left-width:0px; ';
    } //$this->getValue('border_ind','',$id) == 'border_ind'
    else {
      $current_styles .= ($this->getValue('border-width','',$id) != '') ? 'border-width:' . $this->getValue('border-width','',$id) . 'px; ' : 'border-width:0px; ';
    }
    
    return $current_styles;
  }
  
  function message($messages)
  {
    global $post;
    $post_ID = $post->ID;
    
    $messages['custom-styles'] = array(
      0 => '', // Unused. Messages start at index 1.
      1 => sprintf(__('Custom Style updated.')),
      2 => __('Custom field updated.'),
      3 => __('Custom field deleted.'),
      4 => __('Custom Style updated.'),
      /* translators: %s: date and time of the revision */
      5 => isset($_GET['revision']) ? sprintf(__('Custom Style restored to revision from %s'), wp_post_revision_title((int) $_GET['revision'], false)) : false,
      6 => sprintf(__('Custom Style created.')),
      7 => __('Custom Style saved.'),
      8 => sprintf(__('Custom Style submitted. <a target="_blank" href="%s">Preview post</a>'), esc_url(add_query_arg('preview', 'true', get_permalink($post_ID)))),
      9 => sprintf(__('Custom Style scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview post</a>'), 
      // translators: Publish box date format, see http://php.net/date
        date_i18n(__('M j, Y @ G:i'), strtotime($post->post_date)), esc_url(get_permalink($post_ID))),
      10 => sprintf(__('Custom Style draft updated.'))
    );
    
    return $messages;
    
  }
	
	// Remove Quickedit
	function remove_quick_edit( $actions ) {
		global $post;
			if( $post->post_type == 'custom-styles' ) {
			unset($actions['inline hide-if-no-js']);
		}
			return $actions;
	}

  /**
   * PHP 5 Constructor
   */
  function __construct()
  {
    //register_activation_hook( __FILE__, array(  &$this, 'defaults' ) );
    
    add_action('admin_enqueue_scripts', array(
      &$this,
      'add_stylesheets'
    ));
    
    add_action('admin_print_scripts-post-new.php', array(
      &$this,
      'add_admin_scripts'
    ), 10, 1);
    add_action('admin_print_scripts-post.php', array(
      &$this,
      'add_admin_scripts'
    ), 10, 1);
    
    add_action('add_meta_boxes', array(
      &$this,
      'metaboxes'
    ));
    add_action('init', array(
      &$this,
      'post_type'
    ));
    add_filter('mce_css', array(
      &$this,
      'plugin_mce_css'
    ));
    add_action('wp_enqueue_scripts', array(
      &$this,
      'add_dynamic_stylesheet'
    ));
    add_action('template_redirect', array(
      &$this,
      'trigger_check'
    ));
    add_filter('query_vars', array(
      &$this,
      'add_trigger'
    ));
    add_filter('tiny_mce_before_init', array(
      &$this,
      'mce_before_init'
    ), 1000);
    add_filter('mce_buttons_2', array(
      &$this,
      'mce_buttons'
    ), 1000);
    add_action('save_post', array(
      &$this,
      'save_custom_styles_meta'
    ));
    
    add_filter('post_updated_messages', array(
      &$this,
      'message'
    ));
		
		add_filter('post_row_actions', array(
      &$this,
			'remove_quick_edit'
    ),10,2);
    
  }
  /** ======================================= */
  
  
} // End jck_kvt Class

$jck_custom_styles = new jck_custom_styles; // Start an instance of the plugin class